import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "mainLayout",
      },
    },
    {
      path: "/about",
      name: "about",
      components: {
        default: () => import("../views/AboutView.vue"),
        menu: () => import("@/components/menus/AboutMenu.vue"),
        header: () => import("@/components/header/AboutHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/product",
      name: "product",
      components: {
        default: () => import("../views/products/ProductView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "mainLayout",
      },
    },
    {
      path: "/product/full",
      name: "product fullscreen",
      components: {
        default: () => import("../views/products/ProductView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
  ],
});

export default router;
